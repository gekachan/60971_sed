<?php

use Framework\Routing\Router;
use Framework\Routing\Request;

require_once "vendor/autoload.php";

echo (new Router(new Request()))->getContent();
